package cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author jaimin & mit
 */
public class CardDeck {
    private ArrayList<Card1> cards; //created arraylist and passed Card1 as obj
    public CardDeck(ArrayList<Card1> cards) {
        this.cards = cards;
    }

    public CardDeck() {
            this.cards = new ArrayList<Card1>();
    }
        
          
    public String toString() {
    String outputFromCard = "0";
    for(Card1 c1 : cards) {
        outputFromCard = outputFromCard + c1.toString();
    }
    return outputFromCard;
    }
    
    public void shuffle() {
	ArrayList<Card1> temp = new ArrayList<Card1>();
        Random r1 = new Random();
        double r2 = 0;
        double size = cards.size();
        for(int i=0;i < r2 ;i++) {
            r2 = r1.nextInt((cards.size() -1 ));
            temp.add(cards.get((int) r2));
        }
        cards = temp;
     }
      
//this will help to draw card from deck 
        public void drawTheCard(CardDeck output)
        {
            cards.add(output.getCardFromDeck(0));
            
        }

        //this will help to get card from deck and pass to drawTheCard method
    public Card1 getCardFromDeck(int i) {
        return cards.get(i);
    }
	
    public void createDeck() {
        for(Suit s1 : Suit.values()) {
            for(CardNo c1 : CardNo.values()) {
                this.cards.add(new Card1(s1,c1));
            }
	}
            
    }
    
    public int deckSize() {
        return this.cards.size();
    }
    
    
    //return total value of cards in the deck
    public int cardsValue() {
        int totalValue = 0;
        int ace = 0;
        
        for(Card1 aCard : this.cards) {
            switch(aCard.getValue()) {
                case Two: totalValue += 2;
                case Three: totalValue += 3;
                case Four: totalValue += 4;
                case Five: totalValue += 5;
                case Six: totalValue += 6;
                case Seven: totalValue += 7;
                case Eight: totalValue += 8;
                case Nine: totalValue += 9;
                case Ten: totalValue += 10;
                case Jack: totalValue += 10;
                case Queen: totalValue += 10;
                case King: totalValue += 10;
                case Ace: totalValue += 1;      
            }
        }
        
        for(int i = 0; i < ace; i++) {
            if(totalValue > 10) {
                totalValue += 1;
            }
            else {
                totalValue += 11;
            }
        }
        
        return totalValue;
    }
    

}