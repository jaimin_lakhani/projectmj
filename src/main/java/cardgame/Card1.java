package cardgame;


/**
 *
 * @author jaimin & mit
 */

public class Card1 
{
 	private Suit suit;  //for enum made to get all 4 suits
	private CardNo cardNo;  //enum made to get all cards 

	public CardNo getValue() {
            return this.cardNo;
	}

    public Card1(Suit suit, CardNo cardNo) {
        this.suit = suit;
        this.cardNo = cardNo;
    }

    public String toString() {
      return this.suit.toString() + "-" + this.cardNo.toString();
    }

}
    

