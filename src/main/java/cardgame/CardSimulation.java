package cardgame;

import java.util.Scanner;

/**
 *
 * @author jaimin & mit
 */
public class CardSimulation 
{
   public static void main(String[]args)
   {
       System.out.println("Welcome to our project ");
       System.out.println("*-*-*-*-*-*-*-*-*-*-*-");
       
       CardDeck cd1 = new CardDeck();
       cd1.createDeck();
       cd1.shuffle();
       
       
       CardDeck playerSide = new CardDeck();
       CardDeck dealerSide = new CardDeck();
       
       double playerMoney = 100.00;
       Scanner sc = new Scanner(System.in);
       
       //Game loop
        while(playerMoney > 0) {
            System.out.println("You have $" + playerMoney + ", how much would you like to bet?");
            double playerBet = sc.nextDouble();
            if(playerBet > playerMoney) {
                System.out.println("You can't bet more than you have!");
                break;
            }
            
            //dealing the cards for player
            playerSide.drawTheCard(cd1);
            playerSide.drawTheCard(cd1);
            
            //dealing the cards for the dealer
            dealerSide.drawTheCard(cd1);
            dealerSide.drawTheCard(cd1);
            
            while(true) {
                System.out.println("Your hand is:");
                System.out.print(playerSide.toString());
                System.out.println("Your deck's value is: " + playerSide.cardsValue());
                
                //Dealer's hand
                System.out.println("Dealer's Hand: " + dealerSide.getCardFromDeck(0).toString() + "and [Hidden]");
                
                //what does the player want to do:
                System.out.println("Would you like to Hit or Stand? (1) for hit (2) for stand");
                int response = sc.nextInt();
                
                //if they hit
                if(response == 1) {
                    playerSide.drawTheCard(cd1);
                    System.out.println("You draw a: " + playerSide.getCardFromDeck(playerSide.deckSize()-1).toString());
                    //bust if > 21
                    if(playerSide.cardsValue() > 21) {
                        System.out.println("Bust: Card's value is: " + playerSide.cardsValue());
                        playerMoney -= playerBet;
                        //endRound = true;
                        break;
                    }   
                }
                
                if(response == 2) {
                    break;
                }
            }
            
            //Dealers card
            System.out.println("Dealer's cards: " + dealerSide.toString());
            
            //if the dealer has more points than player
            if((dealerSide.cardsValue() > playerSide.cardsValue())) {
                System.out.println("Dealer won! ");
                playerMoney -= playerBet;
            }
            
            while((dealerSide.cardsValue() < 17)) {
                dealerSide.drawTheCard(cd1);
                System.out.print("Dealer draws: " + dealerSide.getCardFromDeck(dealerSide.deckSize()-1).toString());
            }
            
            //Display total value
            System.out.println("Dealer's hand is: " + dealerSide.cardsValue());
            
            //if dealer busted
            if(dealerSide.cardsValue() > 21) {
                System.out.println("Dealer busts! You win!");
                playerMoney += playerBet;
            }
            
            if(playerSide.cardsValue() == dealerSide.cardsValue()) {
                System.out.println("Push");
            }
            
            if(playerSide.cardsValue() > dealerSide.cardsValue()) {
                System.out.println("You win!");
                playerMoney += playerBet;
            }
        }
        
        System.out.println("Game over! You don't have enough money!");
       
    
   }
    
}
